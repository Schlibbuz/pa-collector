const mongoose = require('mongoose');
const r = require('express').Router();


r.get('/', async (req, res) => {
    const result = await mongoose.connection.db.collection('tournaments').find({}).sort({ created: -1 }).toArray();
    res.send(JSON.stringify(result));
});

module.exports = r;

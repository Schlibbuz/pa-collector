/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.pa.collector.jpa;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * <p>Composite-key for UnitVersion.</p>
 * @author Stefan
 */
@Embeddable
public class UnitVersionId implements Serializable {


    private static final long serialVersionUID = 1L;


    /**
     * <p>Foreign-constraint to units.id.</p>
     */
    @Column(nullable = false)
    private long unitId;


    /**
     * <p>Foreign-constraint to versions.id.</p>
     */
    @Column(nullable = false)
    private long versionId;


    /**
     * <p>Constructor.</p>
     */
    public UnitVersionId() {

    }


    /**
     * <p>Constructor.</p>
     * @param unitId the unitId
     * @param versionId  the versionId
     */
    public UnitVersionId(final long unitId, final long versionId) {
        this.unitId = unitId;
        this.versionId = versionId;
    }


    /**
     * <p>Getter.</p>
     * @return the unitId
     */
    public long getUnitId() {
        return unitId;
    }


    /**
     * <p>Setter.</p>
     * @param unitId the unitId
     */
    public void setUnitId(final long unitId) {
        this.unitId = unitId;
    }


    /**
     * <p>Getter.</p>
     * @return the versionId
     */
    public long getVersionId() {
        return versionId;
    }


    /**
     * <p>Setter.</p>
     * @param versionId the versionId
     */
    public void setVersionId(final long versionId) {
        this.versionId = versionId;
    }


    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.unitId);
        hash = 17 * hash + Objects.hashCode(this.versionId);
        return hash;
    }


    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnitVersionId other = (UnitVersionId) obj;
        if (!Objects.equals(this.unitId, other.unitId)) {
            return false;
        }
        if (!Objects.equals(this.versionId, other.versionId)) {
            return false;
        }
        return true;
    }


    @Override
    public String toString() {
        return "UnitVersionId{" + "unitId=" + unitId + ", versionId=" + versionId + '}';
    }

}


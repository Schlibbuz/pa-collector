/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.pa.collector;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.schlibbuz.pa.collector.jpa.Unit;
import org.schlibbuz.pa.collector.jpa.UnitVersion;
import org.schlibbuz.pa.collector.jpa.UnitVersionId;
import org.schlibbuz.pa.collector.jpa.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

/**
 * <p>The scraper implementation.</p>
 * @author Stefan
 */
public class ScraperWorker implements Callable<ScrapeResult> {


    /**
     * <p>Format-settings for json.</p>
     */
    private static final short JSON_FORMAT_INDENT = 4;


    /**
     * <p>Logger.</p>
     */
    private final Logger log = LoggerFactory.getLogger(ScraperWorker.class);


    /**
     * <p>The actual task the worker is on.</p>
     */
    private final ScrapeTask scrapeTask;


    /**
     * <p>A reference to the manager.</p>
     */
    private final ScraperManager manager;


    /**
     * <p>Statistics about the task.</p>
     */
    private final ScrapeResult scrapeResult;


    /**
     * <p>Constructor.</p>
     * @param scrapeTask the current task
     * @param manager the manager reference
     */
    public ScraperWorker(final ScrapeTask scrapeTask, final ScraperManager manager) {
        this.scrapeTask = scrapeTask;
        this.manager = manager;
        scrapeResult = new ScrapeResult();
    }


    /**
     * <p>Correct uncommon urls.</p>
     * @param url the unsane url
     * @return the sane url
     */
    private String sanitizeUrl(final String url) {

        if (!url.contains("?")) {
            String saneUrl = url.concat("?version=titans");
            log.trace("sanitizing " + url + " into " + saneUrl);
            return saneUrl;
        }

        return url;
    }


    /**
     * <p>Convert a unit-detailpage-url to the url where the json can be found.</p>
     * @param url the url to convert
     * @return the converted url
     */
    private String toJsonUrl(final String url) {
        return url.replace("/unit/", "/json/");
    }


    /**
     * <p>Get a filename from a url to store file in local cache.</p>
     * @param url the url
     * @return the filename used by download()
     */
    private String getFileNameFromUrl(final String url) {
        //url is already checked for malformed at this point.
        String fileName = url.substring(url.lastIndexOf("/") + 1)
                .replace("?version=", "-")
                .replace(":", "-")
                .concat(".html");
        if (fileName.startsWith("-")) {
            fileName = "index" + fileName;
        }
        log.trace("conversion: " + url + " -> " + fileName);
        return fileName;
    }


    /**
     * <p>Extract the version-string from a url. This is specific to palobby.</p>
     * @param url the url
     * @return the version-string
     */
    private String getVersionNameFromUrl(final String url) {
        return sanitizeUrl(url)
                .split("\\?version=")[1];
    }


    /**
     * <p>Extract a unit-slug(url-conform short-name) from a url.</p>
     * @param url the url
     * @return the unit-slug
     */
    private String getUnitSlugFromUrl(final String url) {
        String slug = sanitizeUrl(url)
                .split("/units/json/")[1]
                .split("\\?")[0];
        log.trace("generated unit-slug -> " + slug);
        return slug;
    }


    /**
     * <p>Execute a Jsoup-Request.</p>
     * @param url the url
     * @return the response from jsoup
     * @throws Exception when shit happens
     */
    private Response jsoupExecute(final String url) throws Exception {
        log.trace("connecting... -> " + url);
        long httpConStart = System.currentTimeMillis();
        Connection httpCon = Jsoup.connect(url);
        long httpConDone = System.currentTimeMillis();
        log.trace("connected in " + (httpConDone - httpConStart) + "ms!");
        Response resp = httpCon.execute();
        long httpReqRecv = System.currentTimeMillis();
        log.trace(
                "response took " + (httpReqRecv - httpConDone)
                + "ms, was -> " + resp.statusCode()
                + " (" + resp.bodyAsBytes().length + " bytes of type " + resp.contentType() + ")"
        );
        ScraperWorkerStatus status = manager.getScraperWorkerStatus(
                scrapeTask.getUuid()
        );
        status.setLastVisitedStatusCode(
                (short) resp.statusCode()
        );
        status.setLastVisitedUrl(url);
        return resp;
    }


    /**
     * <p>Download a document. Save it in cache if setting is enabled.</p>
     * @param rawUrl the url
     * @return the org.jsoup.nodes.Document
     * @throws Exception when shit happens
     */
    private Document download(final String rawUrl) throws Exception {
        final String url = sanitizeUrl(rawUrl);

        if (manager.getScraperWorkerConfig().useCache()) {
            String fileName = getFileNameFromUrl(url);
            File cacheFile = new File(
                    ".pa-collector/cache/".concat(fileName)
            );
            if (cacheFile.exists()) {
                log.trace("cache-hit for " + url + " -> " + cacheFile.getAbsolutePath());
                ScraperWorkerStatus status = manager.getScraperWorkerStatus(scrapeTask.getUuid());
                status.setLastVisitedStatusCode((short) HttpStatus.NOT_MODIFIED.value());
                status.setLastVisitedUrl("cache-hit :) " + System.currentTimeMillis());
                return Jsoup.parse(cacheFile, "UTF-8", "https://palobby.com/");
            }

            Response resp = jsoupExecute(url);
            log.trace("caching url " + url + " -> " + cacheFile.getAbsolutePath());
            FileUtils.writeStringToFile(cacheFile, resp.body(), "UTF-8");
            return resp.parse();
        }

        return jsoupExecute(url).parse();
    }


    @Override
    public ScrapeResult call() throws Exception {
        String url = scrapeTask.getUrl();
        UUID uuid = scrapeTask.getUuid();
        log.info("scrape " + uuid + " started for url " + url);
        Document doc = download(url);

        Elements versions = doc.select("li > a[href^=/units/?version=]");


        final Set<String> malformedUrls = new HashSet<>();
        List<String> versionUrls = versions
                .stream()
                .map(element -> element.absUrl("href"))
                .distinct()
                .sorted()
                .map(versionUrl -> {
                    try {
                        return new URL(versionUrl).toString();
                    } catch (MalformedURLException e) {
                        log.warn(e.getMessage());
                        malformedUrls.add(versionUrl);
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        versionUrls.stream().forEach(versionUrl ->
            log.trace("found -> " + versionUrl)
        );

        List<String> unitUrls = new ArrayList<>();
        SortedSet<String> unitSlugs = new TreeSet<>();
        for (String versionUrl : versionUrls) {
            try {
                manager.getVersionRepo().save(
                        new Version(
                                getVersionNameFromUrl(versionUrl)
                        )
                );
            } catch (Exception e) {
                log.info(e.toString());
            }

            log.trace("connection to " + versionUrl.toString());
            doc = download(versionUrl);
            Elements unitsInVersion = doc.select("li > a[href^=/units/unit/]");
            for (Element unitInVersion : unitsInVersion) {
                String urlString = toJsonUrl(
                        unitInVersion.absUrl("href")
                );

                try {
                    log.trace("parsing url -> " + unitInVersion.absUrl("href"));
                    new URL(urlString);
                    log.trace("success");
                    unitUrls.add(urlString);
                    unitSlugs.add(getUnitSlugFromUrl(urlString));
                } catch (MalformedURLException e) {
                    log.warn("malformed url -> " + e.getMessage());
                }
                log.trace(unitInVersion.absUrl("href"));
            }

        }
        //distinct urls
        unitUrls = unitUrls.stream().distinct().collect(Collectors.toList());

        unitSlugs.stream().forEach(slug -> {
            try {
                manager.getUnitRepo().save(new Unit("", slug));
            } catch (Exception e) {
                log.info(e.toString());
            }
        });

        unitUrls.stream().forEach(unitUrl ->
                log.trace("found -> " + unitUrl)
        );

        for (String unitUrl : unitUrls) {
            doc = download(unitUrl);

            String jsonData = doc.select("div.panel-body > pre").first().text();
            log.trace(jsonData);
            try {
                log.trace("parsing json...");
                JSONObject json = new JSONObject(jsonData);
                log.trace("...done!");
                log.trace(json.toString());
                String versionName = getVersionNameFromUrl(unitUrl);
                String unitSlug = getUnitSlugFromUrl(unitUrl);
                Unit unit = manager.getUnitRepo().findBySlug(unitSlug).get(0);
                Version version = manager.getVersionRepo().findByName(versionName).get(0);
                UnitVersionId unitVersionId = new UnitVersionId(unit.getId(), version.getId());
                UnitVersion unitVersion = new UnitVersion(unitVersionId, json.toString(JSON_FORMAT_INDENT));
                unitVersion.setUnit(unit);
                unitVersion.setVersion(version);
                manager.getUnitVersionRepo().save(
                        unitVersion
                );
            } catch (org.springframework.dao.DataIntegrityViolationException | JSONException e) {
                log.error(e.getMessage());
            } catch (Exception e) {
                log.info(e.toString());
            }

        }

        log.info("found a total of " + unitUrls.size() + " urls");
        log.info("scrape " + uuid + " finished");
        manager.archiveScraperWorkerStatus(uuid);
        return scrapeResult;
    }


    /**
     * <p>Get start-ulr of scrape-task.</p>
     * @return the start-url
     */
    public String getUrl() {
        return scrapeTask.getUrl();
    }


    /**
     * <p>Get the scrape-result.</p>
     * @return the scrape-result
     */
    public ScrapeResult getScrapeResult() {
        return scrapeResult;
    }

}

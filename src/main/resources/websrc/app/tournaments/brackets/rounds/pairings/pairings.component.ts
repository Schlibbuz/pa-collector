import { Component, OnInit, Input } from '@angular/core';

import { Pairing } from 'app/tournaments/brackets/rounds/pairings/pairing';

@Component({
    selector: 'app-tournament-bracket-round-pairing',
    templateUrl: './pairings.component.html',
    styleUrls: ['./pairings.component.scss']
})
export class PairingsComponent implements OnInit {

    @Input() pairing: Pairing;

    constructor() { }

    ngOnInit(): void {

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.pa.collector.config;

import org.schlibbuz.pa.collector.ScraperManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * <p>Configure scopes of beans here.
 * </p>
 * @author Stefan
 */
@Configuration
public class ScopesConfig {

    /**
     * <p>Get the ScraperManagers Singleton-Instance.
     * </p>
     * @return the ScraperManager Singleton-Instance
     * @version 0.0.1-SNAPSHOT
     */
    @Bean
    @Scope("singleton")
    public ScraperManager scraperManagerSingleton() {
        return new ScraperManager();
    }
}

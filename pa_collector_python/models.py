"""sqlalchemy models"""

import logging
import os

from logdecorator import (
    log_on_end,
    log_on_error,
    log_on_start,
)
from sqlalchemy import (
    Column,
    create_engine,
    ForeignKey,
    Integer,
    String,
    Text,
)
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


Base = declarative_base()
Session = sessionmaker()
failed_commits = list()


def init_db():
    """init db"""

    engine = create_engine(
        'postgresql+psycopg2://' +
        os.environ['PA_DB_USER'] +
        ':' +
        os.environ['PA_DB_PASS'] +
        '@127.0.0.1:5432/pa',

        echo=True,
    )

    Base.metadata.create_all(engine)
    Session.configure(bind=engine)


class Version(Base):
    """Maps to versions table"""

    __tablename__ = 'py3_versions'

    id = Column(Integer, primary_key=True)
    name = Column(String(50), nullable=False, unique=True)

    def __repr__(self):
        """text repr"""

        return "<Version(id=%s, name='%s')>" % (self.id, self.name)


class Unit(Base):
    """Maps to units table"""

    __tablename__ = 'py3_units'

    id = Column(Integer, primary_key=True)
    name = Column(String(50), default='', nullable=False)
    slug = Column(String(50), nullable=False, unique=True)

    def __repr__(self):
        """text repr"""

        return "<Unit(id=%s, name='%s', slug='%s')>" % (self.id, self.name, self.slug)


class UnitVersion(Base):
    """Maps to units_versions table"""

    __tablename__ = 'py3_units_versions'

    unit_id = Column(Integer, ForeignKey(Unit.id), primary_key=True)
    version_id = Column(Integer, ForeignKey(Version.id), primary_key=True)
    json_data = Column(Text, nullable=False, default='{}')

    def __repr__(self):
        """text repr"""

        return "<UnitVersion(unit_id=%s, version_id='%s', json_data_size=%s)>"\
            % (self.unit_id, self.version_id, len(self.json_data))


class HttpTrace(Base):
    """Maps to http_traces table"""

    __tablename__ = 'py3_http_traces'

    id = Column(Integer, primary_key=True)
    request_url = Column(String(255), nullable=False)
    status_code = Column(Integer, nullable=False)

    def __repr__(self):
        """text repr"""

        return "<HttpTrace(id=%s, request_url='%s', status_code='%s')>"\
            % (self.id, self.request_url, self.status_code)


@log_on_start(logging.DEBUG, "Saving")
@log_on_error(logging.ERROR, "Error on saving: {e!r}",
              on_exceptions=IntegrityError,
              reraise=True)
@log_on_end(logging.DEBUG, "Record saved")
def orm_save(entity):
    """save an object to db"""

    session = Session()

    try:
        session.add(entity)
        session.commit()

    except IntegrityError as err:
        logging.warning(err)
        session.rollback()

    finally:
        session.close()

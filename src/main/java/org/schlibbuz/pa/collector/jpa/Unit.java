/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.pa.collector.jpa;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>Unit entity.</p>
 * @author Stefan
 */
@Entity
@Table(name = "units")
public class Unit implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * <p>Primary-key.</p>
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;


    /**
     * <p>Name.</p>
     */
    @Column(nullable = false, columnDefinition = "varchar(50)")
    private String name;


    /**
     * <p>Slug.</p>
     */
    @Column(nullable = false, unique = true, columnDefinition = "varchar(50)")
    private String slug;


    /**
     * <p>Constructor.</p>
     */
    public Unit() {

    }


    /**
     * <p>Constructor.</p>
     * @param name the name
     * @param slug the slug
     */
    public Unit(final String name, final String slug) {
        this.name = name;
        this.slug = slug;
    }


    /**
     * <p>Getter.</p>
     * @return the id
     */
    public long getId() {
        return id;
    }


    /**
     * <p>Setter.</p>
     * @param id the id
     */
    public void setId(final long id) {
        this.id = id;
    }


    /**
     * <p>Getter.</p>
     * @return the name
     */
    public String getName() {
        return name;
    }


    /**
     * <p>Setter.</p>
     * @param name the name
     */
    public void setName(final String name) {
        this.name = name;
    }


    /**
     * <p>Getter.</p>
     * @return the slug
     */
    public String getSlug() {
        return slug;
    }


    /**
     * <p>Setter.</p>
     * @param slug the slug
     */
    public void setSlug(final String slug) {
        this.slug = slug;
    }

    @SuppressWarnings("checkstyle:DesignForExtension")
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.id);
        hash = 53 * hash + Objects.hashCode(this.name);
        hash = 53 * hash + Objects.hashCode(this.slug);
        return hash;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Unit other = (Unit) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.slug, other.slug)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Unit{" + "id=" + id + ", name=" + name + ", slug=" + slug + '}';
    }

}


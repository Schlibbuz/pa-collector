/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.pa.collector;

import java.util.UUID;

/**
 * <p>A task-definition for a scrape.</p>
 * @author Stefan
 */
public class ScrapeTask {


    /**
     * <p>start-url.</p>
     */
    private final String url;


    /**
     * <p>uuid.</p>
     */
    private final UUID uuid;


    /**
     * <p>Constructor.</p>
     * @param url the start-url
     */
    public ScrapeTask(final String url) {
        this.url = url;
        uuid = UUID.randomUUID();
    }


    /**
     * <p>Getter.</p>
     * @return the start-url
     */
    public String getUrl() {
        return url;
    }


    /**
     * <p>Getter.</p>
     * @return the uuid of the scrape-task
     */
    public UUID getUuid() {
        return uuid;
    }

}

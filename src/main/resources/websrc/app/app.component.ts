import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from 'app/_services/auth.service';
import { Tournament } from './tournaments/tournament';
import { TournamentsService } from './tournaments/tournaments.service';
import { User } from 'app/_models/user';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    providers: [
        TournamentsService,
    ],
})
export class AppComponent implements OnInit {
    currentUser: User;
    showUnitsDb = false;
    title = 'Unicorn tournament series';
    tournaments: Array<Tournament>;

    constructor(
        private authService: AuthService,
        private router: Router,
        private tournamentsService: TournamentsService,
    ) {
        this.authService.currentUser.subscribe(x => this.currentUser = x);
    }


    ngOnInit(): void {
        this.getTournaments();
    }

    getTournaments(): void {
        this.tournamentsService.getTournaments()
            .subscribe(tournaments => (this.tournaments = tournaments));
    }

    toggleUnitsDb(showUnitsDb: boolean): void {
        this.showUnitsDb = showUnitsDb;
    }

    logout(): void {
        this.authService.logout();
        this.router.navigate(['']);
    }
}

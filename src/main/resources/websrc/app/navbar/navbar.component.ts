import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { Router } from '@angular/router';

import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { AuthService } from 'app/_services/auth.service';
import { Tournament } from '../tournaments/tournament';
import { User } from 'app/_models/user';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss'],

})
export class NavbarComponent implements OnInit {

    @Output() showUnitsDb = new EventEmitter<boolean>();
    @Input() tournaments: Tournament[];
    currentUser: User;
    isVisUnitsDb = false;
    loginOpen = false;
    navbarOpen = false;
    newUser = false;
    closeResult: string;

    constructor(
        private authService: AuthService,
        private router: Router,
        private modalService: NgbModal,
    ) {
        this.authService.currentUser.subscribe(x => this.currentUser = x);
    }

    ngOnInit(): void { }

    toggleProfile(): void {
        this.loginOpen = !this.loginOpen;
    }

    // this is for mobile
    toggleNavbar(): void {
        this.navbarOpen = !this.navbarOpen;
    }

    resetNewUser(): void {
        this.newUser = false;
    }

    toggleNewUser(): void {
        this.newUser = !this.newUser;
    }

    toggleUnitsDb(): void {
        this.isVisUnitsDb = !this.isVisUnitsDb;
        this.showUnitsDb.emit(this.isVisUnitsDb);
    }

    openModal(content): void {
        this.toggleProfile();
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
            this.resetNewUser();
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            this.resetNewUser();
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    logout(): void {
        this.authService.logout();
        this.router.navigate(['']);
    }

}

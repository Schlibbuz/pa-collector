import { Component, Input, OnInit } from '@angular/core';

import { Player } from 'app/players/player';
import { Tournament } from './tournament';


@Component({
    selector: 'app-tournament',
    templateUrl: './tournaments.component.html',
    styleUrls: ['./tournaments.component.scss'],
})
export class TournamentsComponent implements OnInit {
    @Input() tournament: Tournament;
    editTournament: Tournament;
    rounds: string[];
    winner: Player;

    constructor() {
        this.winner = {name: '[ICARUS] Nimzo', league: 'uber'};
    }

    ngOnInit(): void {

    }

}

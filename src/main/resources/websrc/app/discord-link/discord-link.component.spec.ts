import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiscordLinkComponent } from './discord-link.component';

describe('DiscordLinkComponent', () => {
  let component: DiscordLinkComponent;
  let fixture: ComponentFixture<DiscordLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiscordLinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiscordLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

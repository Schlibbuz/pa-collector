import { Pairing } from 'app/tournaments/brackets/rounds/pairings/pairing';

export interface Round {
    index: number;
    name: string;
    pairings: Array<Pairing>;
}

import { Deactivation } from './deactivation';

export interface User {
    _id: string;
    created: Date;
    deactivated: Deactivation;
    discord: string;
    email: string;
    name: string;
    password: string;
    token: string;
  }

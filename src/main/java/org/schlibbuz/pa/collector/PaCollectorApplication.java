package org.schlibbuz.pa.collector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * <p>App.</p>
 * @author Stefan
 */
@SpringBootApplication
public class PaCollectorApplication {


    /**
     * <p>Main.</p>
     * @param args the command line arguments
     */
    public static void main(final String[] args) {
        SpringApplication.run(PaCollectorApplication.class, args);
    }

}

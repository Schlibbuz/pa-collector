package org.schlibbuz.pa.collector.jpa;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;


/**
 * <p>Integeration-test for version-repo.</p>
 * @author Stefan
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class VersionRepoIntegrationTest {


    /**
     * <p>TestEntityManager.</p>
     */
    @Autowired
    private TestEntityManager entityManager;


    /**
     * <p>VersionRepo.</p>
     */
    @Autowired
    private VersionRepo versionRepo;


    /**
     * <p>Test Version-entity.</p>
     */
    @Test
    public void whenFindByNameThenReturnVersion() {
        // given
        Version classic = new Version("classic");
        entityManager.persist(classic);
        entityManager.flush();

        // when
        Version found = versionRepo.findByName(classic.getName()).get(0);

        // then
        assertThat(found.getName())
          .isEqualTo(classic.getName());
    }

}

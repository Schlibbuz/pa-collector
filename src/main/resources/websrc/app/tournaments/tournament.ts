import { Bracket } from 'app/tournaments/brackets/bracket';
import { Player } from 'app/players/player';

export interface Tournament {
    date: string;
    time: string;
    format: string;
    rounds: number;
    participants: Array<Player>;
    bracket: Bracket;
    maps: Array<string>;
    mods: Array<string>;
    prizes: Array<string>;
    winner: Player;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.pa.collector;

import java.time.Duration;

/**
 * <p>A worker configuration.</p>
 * @author Stefan
 */
public class ScraperWorkerConfig {


    /**
     * <p>socket and conn timeouts for jsoup.</p>
     */
    private static final byte JSOUP_TIMEOUTS = 18;

    /**
     * <p>Jsoup timeout settings.</p>
     */
    private Duration timeout;


    /**
     * <p>Wether the worker should use the cache.</p>
     */
    private boolean useCache;


    /**
     * <p>Default-Constructor.</p>
     */
    public ScraperWorkerConfig() {
        timeout = Duration.ofSeconds(JSOUP_TIMEOUTS);
    }


    /**
     * <p>Get default worker config.</p>
     * @return the default config
     */
    public static ScraperWorkerConfig getDefault() {
        return new ScraperWorkerConfig()
                .timeout(
                        Duration.ofSeconds(JSOUP_TIMEOUTS)
                )
                .useCache(false);
    }


    /**
     * <p>Get the timeout of the config.</p>
     * @return the timeout
     */
    public Duration timeout() {
        return timeout;
    }


    /**
     * <p>Set the timeout of the config.</p>
     * @param newSetting the new timeout duration
     * @return instance for chaining
     */
    public ScraperWorkerConfig timeout(final Duration newSetting) {
        timeout = newSetting;
        return this;
    }


    /**
     * <p>Enable/disable cache.</p>
     * @param newSetting the choice
     * @return instance for chaining
     */
    public ScraperWorkerConfig useCache(final boolean newSetting) {
        useCache = newSetting;
        return this;
    }


    /**
     * <p>Getter for cache-switch.</p>
     * @return the cache-switch
     */
    public boolean useCache() {
        return useCache;
    }


    /**
     * <p>Get Jsoup timeout-settings.</p>
     * @return the jsoup timeouts
     */
    protected static final short getJsoupTimeouts() {
        return JSOUP_TIMEOUTS;
    }

}

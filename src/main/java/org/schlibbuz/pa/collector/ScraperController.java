/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.pa.collector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>Rest-Controller for Scraper-Management.</p>
 * @author Stefan
 */
@RestController
public class ScraperController {


    /**
     * <p>Service-Injection.</p>
     */
    @Autowired
    private ScraperService scraper;


    /**
     * <p>Maps GET-Requests to "/start".</p>
     * @return a status if start was successful
     */
    @GetMapping("/start")
    public ScraperManagerStatus start() {
        return scraper.start();
    }


    /**
     * <p>Maps GET-Requests to "/status".</p>
     * @return a status about the condition of the scrapers
     */
    @GetMapping("/status")
    public ScraperManagerStatus status() {
        return scraper.status();
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.pa.collector;

/**
 * <p>Result of a scrape-run.</p>
 * @author Stefan
 */
public final class ScrapeResult {


    /**
     * <p>outcome.</p>
     */
    private String outcome;


    /**
     * <p>Constructor.</p>
     */
    public ScrapeResult() {
        this.outcome = "success";
    }


    /**
     * <p>Getter.</p>
     * @return the outcome
     */
    public String getOutcome() {
        return outcome;
    }


    /**
     * <p>Setter.</p>
     * @param outcome the outcome
     */
    public void setOutcome(final String outcome) {
        this.outcome = outcome;
    }

}

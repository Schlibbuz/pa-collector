/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.pa.collector.jpa;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>Version entity.</p>
 * @author Stefan
 */
@Entity
@Table(name = "versions")
public class Version implements Serializable {


    private static final long serialVersionUID = 1L;


    /**
     * <p>Primary-key.</p>
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;


    /**
     * <p>Name column.</p>
     */
    @Column(nullable = false, unique = true, columnDefinition = "varchar(50)")
    private String name;


    /**
     * <p>Constructor.</p>
     */
    public Version() {

    }


    /**
     * <p>Constructor.</p>
     * @param name the name
     */
    public Version(final String name) {
        this.name = name;
    }


    /**
     * <p>Getter.</p>
     * @return the id
     */
    public long getId() {
        return id;
    }


    /**
     * <p>Setter.</p>
     * @param id the id
     */
    public void setId(final long id) {
        this.id = id;
    }


    /**
     * <p>Getter.</p>
     * @return the name
     */
    public String getName() {
        return name;
    }


    /**
     * <p>Setter.</p>
     * @param name the name
     */
    public void setName(final String name) {
        this.name = name;
    }


    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 97 * hash + Objects.hashCode(this.name);
        return hash;
    }


    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Version other = (Version) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }


    @Override
    public String toString() {
        return "Version{" + "id=" + id + ", name=" + name + '}';
    }

}

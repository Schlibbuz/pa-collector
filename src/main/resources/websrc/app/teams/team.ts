import { Player } from 'app/players/player';

export interface Team {
    players: Array<Player>;
    score: number;
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Profile } from '../_models/profile';
import { User } from '../_models/user';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll(): Observable<User[]> {
        return this.http.get<User[]>(`${environment.apiUrl}/api/users`);
    }

    getProfile(): Observable<Profile> {
        return this.http.get<Profile>(`${environment.apiUrl}/api/profile`);
    }
}

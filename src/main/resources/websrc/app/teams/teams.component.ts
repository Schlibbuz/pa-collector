import { Component, OnInit, Input } from '@angular/core';

import { Team } from 'app/teams/team';

@Component({
    selector: 'app-team',
    templateUrl: './teams.component.html',
    styleUrls: ['./teams.component.scss']
})
export class TeamsComponent implements OnInit {

    @Input() team: Team;
    constructor() { }

    ngOnInit(): void {
    }

}

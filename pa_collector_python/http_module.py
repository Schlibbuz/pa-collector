"""networking"""

import logging
import os

from logdecorator import log_on_start, log_on_end, log_on_error
import requests


CACHE_DIR = '.pa-collector/cache'
#CACHE_DIR = False
use_cache = CACHE_DIR != False #pylint: disable=singleton-comparison

failed_requests = list()


@log_on_start(logging.DEBUG, "Getting File-Name from url {url:s}...")
@log_on_end(logging.DEBUG, "Generated filename '{result:s}' from url '{url:s}'")
def filename_from_url(url):
    """generate filename from url"""

    filename = url.rsplit("/", 1)[-1]

    if not filename:
        return "index.html"

    if filename[0] == '?':
        return "index.html" + filename

    return filename


@log_on_start(logging.DEBUG, "Downloading content from url {url:s} to -> {path:s}...")
@log_on_end(logging.DEBUG, "Url '{url:s}' downladed to '{path:s}'")
def download(response, path):
    """download a file to cache"""

    with open(path, 'wb') as file:
        file.write(response.content)


@log_on_start(logging.DEBUG, "Start downloading {url:s}...")
@log_on_error(logging.ERROR, "Error on downloading {url:s}: {e!r}",
              on_exceptions=IOError,
              reraise=True)
@log_on_end(logging.DEBUG,
            "Downloading {url:s} finished successfully within \
                {result.elapsed!s} ({result.status_code:d})")
def request(url):
    """GET-request"""


    if use_cache:
        path = os.path.join(CACHE_DIR, filename_from_url(url))
        if os.path.exists(path):
            # prefer locally cached version

            with open(path, "rb") as file:
                response = requests.models.Response()
                response.code = "not modified"
                response.error_type = "not modified"
                response.status_code = 304
                response._content = file.read() # pylint: disable=protected-access
                return response

    response = requests.get(url)

    if response.status_code != 200:
        failed_requests.append(url + " -> " + response.status_code)

    if use_cache:
        download(response, path)

    return response

# PaCollector

Welcome to PA-Collector,  a showcase of technology in spring, angular, python, node and asp.net core.

[[_TOC_]]

This project was created with this set of tools and technologies.
- Spring-Framework https://spring.io/
- Angular https://angular.io/
- NodeJS https://nodejs.org/
- Tomcat https://tomcat.apache.org/
- Maven https://maven.apache.org/
- Python 3 https://python.org/

## Development server

### Spring
run `mvnw spring-boot:run` for a dev server. Runnig this requires jdk-11.

### Angular
Run `ng serve` for a dev server. Navigate to `http://localhost:9000/`. The app will automatically reload if you change any of the source files.

## REST_API

Run `node app.js` in the folder `trash-backend/`. This will fire up a very basic api connected to your local mongodb. Navigate to `http://localhost:4220/` to see whats up.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

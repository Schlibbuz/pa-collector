const Constants = {
    'Tournaments': [
        {
            date: 'July 12th',
            time: '17:00 GMT',
            format: 'Best of 3 double elimination bracket',
            participants: [
                '【W】[Unicorn] QH',
                '[BSE][Unicorn] Makocb',
                'jerle-apeño',
                'TheEffectTheCause ',
                'Academia *',
                'Fhuj',
                '【W】Viroo *',
                '【W】Ziroo *',
            ],
            maps: [
                'The Choke 1v1 shared',
                'Diadect 1v1 shared',
                'ROC',
                'the classy barren 1v1',
                'niflhel',
                'tropic',
                'tournament gas 1v1 shared',
                'pacific',
                'PAF 5p FFA',
                'PAX',
                'Decider in the finals will be on a secret system',
            ],
            mods: [
                'Super Commander mod',
            ],
            prizes: [
                'Unicorn-Commander for the winner',
            ],
        },
        {
            date: 'July 19th',
            time: '17:00 GMT',
            format: 'Best of 3 double elimination bracket',
            participants: [

            ],
            maps: [

            ],
            mods: [

            ],
            prizes: [

            ],
        },
        {
            date: 'July 26th',
            time: '17:00 GMT',
            format: 'Best of 3 double elimination bracket',
            participants: [

            ],
            maps: [

            ],
            mods: [

            ],
            prizes: [

            ],
        },
        {
            date: 'August 2nd',
            time: '17:00 GMT',
            format: 'Best of 3 double elimination bracket',
            participants: [

            ],
            maps: [

            ],
            mods: [

            ],
            prizes: [

            ],
        },
        {
            date: 'August 9nd',
            time: '17:00 GMT',
            format: 'Best of 3 double elimination bracket',
            participants: [

            ],
            maps: [

            ],
            mods: [

            ],
            prizes: [

            ],
        },
    ]
};

module.exports = Constants;

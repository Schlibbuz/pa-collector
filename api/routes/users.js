const mongoose = require('mongoose');
const r = require('express').Router();

const auth = require('../helpers/verifyJwt');
const User = require('../models/user');


r.get('/', auth, async (req, res) => {
    res.send(await User.find());
});

module.exports = r;

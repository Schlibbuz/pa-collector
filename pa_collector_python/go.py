"""
script to gather json-data about units and structures from palobby.com
"""

import json
import logging
from urllib.parse import urljoin

from lxml import html

from http_module import (
    failed_requests,
    request,
)
from models import (
    Version,
    Unit,
    UnitVersion,
    orm_save,
    Session,
    init_db,
)


BASE_URL = "https://palobby.com/"


def init():
    """init"""

    init_logger()
    init_db()


def init_logger():
    """init logger"""

    logging.basicConfig(
        filename=".pa-collector/app.log",
        level=logging.DEBUG,
    )


def get_units(version_string):
    """get all units in a specific version"""

    response = request(
        urljoin(BASE_URL, "/units/?version=" + version_string)
    )
    dom = html.fromstring(response.content)
    return sorted(
        set(
            dom.xpath('//a[contains(@href, "/units/unit/")]/@href')
        )
    )



def get_unit_json(unit_slug, version_string):
    """get units json data"""

    response = request(
        urljoin(
            BASE_URL,
            "/units/json/" +
            unit_slug +
            "?version=" +
            version_string,
        )
    )
    dom = html.fromstring(response.content)
    return dom.xpath('//div[@class="panel-body"]/pre/text()')


def get_unit_slug(url):
    """get unit slug"""

    return url.split("?")[0][12:]


def get_versions():
    """get version-strings of the different versions pa has"""

    response = request(
        urljoin(BASE_URL, "/units/")
    )
    dom = html.fromstring(response.content)

    return sorted(
        set(
            dom.xpath('//a[contains(@href, "/units/?version=")]/@href')
        )
    )


def main():
    """main function"""

    init()

    versions = get_versions()

    unit_urls = set()

    for version in versions:
        version_string = version[16:]
        orm_save(Version(name=version_string))
        unit_urls.update(get_units(version_string))

    unit_urls = sorted(unit_urls)

    unit_slugs = set(
        map(get_unit_slug, unit_urls)
    )

    for unit_slug in unit_slugs:
        orm_save(Unit(slug=unit_slug))


    logging.warning("%d records to fetch", len(unit_urls))

    session = Session()

    for unit_url in unit_urls:
        unit_json_url = unit_url.replace("/unit/", "/json/", 1)
        response = request(
            urljoin(BASE_URL, unit_json_url)
        )
        dom = html.fromstring(response.content)
        xpath_result_string = "".join(
            dom.xpath('//div[@class="panel-body"]/descendant::text()')
        )
        json_data = json.loads(xpath_result_string)
        unit_version_pair = unit_url.rsplit("/", 1)[1].split("?version=")
        orm_save(
            UnitVersion(
                unit_id=session.query(Unit).filter_by(slug=unit_version_pair[0]).first().id,
                version_id=session.query(Version).filter_by(name=unit_version_pair[1]).first().id,
                json_data=json.dumps(json_data, sort_keys=True, indent=4),
            )
        )

    session.close()

    for failed_request in failed_requests:
        print(failed_request)


if __name__ == "__main__":
    main()

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.pa.collector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>ScraperService implementation.</p>
 * @author Stefan
 */
@Service
public class ScraperService implements IScraperService {


    /**
     * <p>Injected depenency.</p>
     */
    @Autowired
    private ScraperManager scraperManager;


    /**
     * <p>Start a scrape task, or at least try it.</p>
     * @return the status/health/mood of the manager
     */
    @Override
    public ScraperManagerStatus start() {

        scraperManager.schedule(
                new ScrapeTask("https://palobby.com/units/")
        );
        return scraperManager.getStatus();
    }


    /**
     * <p>Ask for the status of the manager.</p>
     * @return the status/health/mood of the manager
     */
    @Override
    public ScraperManagerStatus status() {
        return scraperManager.getStatus();
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.pa.collector.jpa;

import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

/**
 * <p>Access and modify specific units.</p>
 * @author Stefan
 */
public interface UnitVersionRepo extends CrudRepository<UnitVersion, UnitVersionId> {


    /**
     * <p>Find Unit-Version by id.</p>
     * @param id the id
     * @return the result or null
     */
    Optional<UnitVersion> findById(UnitVersionId id);
}

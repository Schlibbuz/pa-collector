import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnitsDbComponent } from './units-db.component';

describe('UnitsDbComponent', () => {
  let component: UnitsDbComponent;
  let fixture: ComponentFixture<UnitsDbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnitsDbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnitsDbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

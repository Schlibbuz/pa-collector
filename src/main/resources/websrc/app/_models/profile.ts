export interface Profile {
    userId: string;
    modified: Date;
    roles: Array<string>;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.pa.collector;

import java.util.UUID;

/**
 * <p>Info about teh status of a scrape-task.</p>
 * @author Stefan
 */
public class ScraperWorkerStatus {


    /**
     * <p>uuid.</p>
     */
    private UUID uuid;


    /**
     * <p>Last visited url.</p>
     */
    private String lastVisitedUrl;


    /**
     * <p>Last received http-status-code.</p>
     */
    private short lastVisitedStatusCode;


    /**
     * <p>Constructor.</p>
     * @param uuid the uuid
     * @param lastVisitedUrl the last visited url
     * @param lastVisitedStatusCode the last received http-status-code
     */
    public ScraperWorkerStatus(final UUID uuid, final String lastVisitedUrl, final short lastVisitedStatusCode) {
        this.uuid = uuid;
        this.lastVisitedUrl = lastVisitedUrl;
        this.lastVisitedStatusCode = lastVisitedStatusCode;
    }


    /**
     * <p>Getter.</p>
     * @return the uuid of the scrape-task
     */
    public UUID getUuid() {
        return uuid;
    }


    /**
     * <p>Setter.</p>
     * @param uuid the uuid
     */
    public void setUuid(final UUID uuid) {
        this.uuid = uuid;
    }


    /**
     * <p>Getter.</p>
     * @return the last visited url
     */
    public String getLastVisitedUrl() {
        return lastVisitedUrl;
    }


    /**
     * <p>Setter.</p>
     * @param lastVisitedUrl the last visited url
     */
    public void setLastVisitedUrl(final String lastVisitedUrl) {
        this.lastVisitedUrl = lastVisitedUrl;
    }


    /**
     * <p>Getter.</p>
     * @return the last received http-status-code
     */
    public short getLastVisitedStatusCode() {
        return lastVisitedStatusCode;
    }


    /**
     * <p>Setter.</p>
     * @param lastVisitedStatusCode the last received http-status-code
     */
    public void setLastVisitedStatusCode(final short lastVisitedStatusCode) {
        this.lastVisitedStatusCode = lastVisitedStatusCode;
    }

}

import { Component, Input, OnInit } from '@angular/core';

import { Pairing } from 'app/tournaments/brackets/rounds/pairings/pairing';
import { Player } from 'app/players/player';
import { Round } from 'app/tournaments/brackets/rounds/round';
import { Team } from 'app/teams/team';


@Component({
    selector: 'app-tournament-bracket-round',
    templateUrl: './rounds.component.html',
    styleUrls: ['./rounds.component.scss']
})
export class RoundsComponent implements OnInit {

    // @Input() pairings: Pairing[];
    @Input() round: Round;

    // team1: Team;
    // team2: Team;
    // playerNimzo: Player;
    // playerTETC: Player;


    constructor() {
        // this.playerNimzo = {name: '[ICARUS] Nimzo', league: 'uber'};
        // this.playerTETC = {name: 'TETC', league: 'plat'};
        // this.team1 = { players: [this.playerNimzo]};
        // this.team2 = { players: [this.playerTETC]};

        // this.pairings = [
        //     {
        //         team1: this.team1,
        //         team2: this.team2,
        //     }
        // ];

    }

    ngOnInit(): void {

    }

}

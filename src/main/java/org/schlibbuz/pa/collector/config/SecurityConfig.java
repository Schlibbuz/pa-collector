/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.pa.collector.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * <p>Configure security things here.
 * </p>
 * @author Stefan
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * <p>Configure authorization here.
     * </p>
     * @param http the http-security-object
     * @version 0.0.1-SNAPSHOT
     */
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests().antMatchers("/*").permitAll();
    }
}

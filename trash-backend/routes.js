'use strict';
module.exports = function(app) {
  const tournaments = require('./tournamentsController');

  // todoList Routes
  app.route('/api/tournaments')
    .get(tournaments.getAllTournaments);


  app.route('/api/tournaments/:tournamentId')
    .get(tournaments.getTournament);
};

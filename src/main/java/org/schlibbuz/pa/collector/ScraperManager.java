/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.pa.collector;

import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.schlibbuz.pa.collector.jpa.UnitRepo;
import org.schlibbuz.pa.collector.jpa.UnitVersionRepo;
import org.schlibbuz.pa.collector.jpa.VersionRepo;


/**
 * <p>Manages Scraper-Workers.</p>
 * @author Stefan
 */
public class ScraperManager {
    /**
     * <p>Max numer of concurrent threads.</p>
     */
    private static final byte MAX_CONCURRENT_SCRAPERS = 4;


    /**
     * <p>Logger.</p>
     */
    private final Logger log = LoggerFactory.getLogger(ScraperManager.class);


    /**
     * <p>Thread control.</p>
     */
    private final ExecutorService executor;


    /**
     * <p>Status.</p>
     */
    private final ScraperManagerStatus status;


    /**
     * <p>Config.</p>
     */
    private final ScraperWorkerConfig scraperWorkerConfig;


    /**
     * <p>jpa units.</p>
     */
    @Autowired private UnitRepo unitRepo;


    /**
     * <p>jpa versions.</p>
     */
    @Autowired private VersionRepo versionRepo;


    /**
     * <p>jpa unit-versions.</p>
     */
    @Autowired private UnitVersionRepo unitVersionRepo;


    /**
     * <p>Default-Constructor.</p>
     */
    public ScraperManager() {
        executor = Executors.newFixedThreadPool(MAX_CONCURRENT_SCRAPERS);
        status = new ScraperManagerStatus().hasCapacity(true);
        scraperWorkerConfig = new ScraperWorkerConfig()
                                    .useCache(true);
    }


    /**
     * <p>Getter.</p>
     * @return the status
     */
    public ScraperManagerStatus getStatus() {
        return status;
    }


    /**
     * <p>Getter.</p>
     * @return the sharedconfig of the scraper-workers
     */
    public ScraperWorkerConfig getScraperWorkerConfig() {
        return scraperWorkerConfig;
    }


    /**
     * <p>Getter.</p>
     * @param uuid the uuid of the scrape-task
     * @return the status of the worker working on scrape-task with uuid
     */
    public ScraperWorkerStatus getScraperWorkerStatus(final UUID uuid) {
        return status.getScraperWorkerStatus(uuid);
    }


    /**
     * <p>Return number of active threads.</p>
     * @return the active threads
     */
    public int activeThreads() {
        int activeScrapers = ((ThreadPoolExecutor) executor).getActiveCount();
        log.info("active scrapers -> " + activeScrapers + " out of " + MAX_CONCURRENT_SCRAPERS + "(max)");
        return activeScrapers;
    }


    /**
     * <p>Tells if the manager has capacity to accept more tasks.</p>
     * @return the answer in yes/no format
     */
    public int hasCapacity() {
        int activeScrapers = activeThreads();
        return MAX_CONCURRENT_SCRAPERS - activeScrapers;
    }


    /**
     * <p>Schedule a scrape-task with the manager.</p>
     * @param scrapeTask the task to schedule
     * @return the uuid a.k.a. your ticket-number
     */
    public synchronized UUID schedule(final ScrapeTask scrapeTask) {
        // safety-barrier here?
        UUID uuid = scrapeTask.getUuid();
        log.info("scheduling new ScrapeTask(" + uuid + ") -> " + scrapeTask.getUrl());
        int capacity = hasCapacity();
        if (capacity > 0) {
            if (capacity == 1) {
                status.setHasCapacity(false);
            } else {
                status.setHasCapacity(true);
            }
            ScraperWorkerStatus scraperWorkerStatus = new ScraperWorkerStatus(uuid, "uninitialized", (short) 0);
            status.addScraperWorkerStatus(uuid, scraperWorkerStatus);
            @SuppressWarnings("unused")
            Future<ScrapeResult> result = executor.submit(
                new ScraperWorker(scrapeTask, this)
            );
            log.info("done");
            return uuid;
        }
        log.info("no capacity, sorry!");
        return null;
    }


    /**
     * <p>Archive a finished/cancelled scrape-task.</p>
     * @param uuid the task to process
     */
    public void archiveScraperWorkerStatus(final UUID uuid) {
        status.archiveScraperWorkerStatus(uuid);
    }


    /**
     * <p>Shutdown the thread-executor-pool.</p>
     */
    public void shutdown() {
        executor.shutdown();
    }


    /**
     * <p>Get unit repo.</p>
     * @return the unitRepo
     */
    protected UnitRepo getUnitRepo() {
        return unitRepo;
    }


    /**
     * <p>Get version repo.</p>
     * @return the versionRepo
     */
    protected VersionRepo getVersionRepo() {
        return versionRepo;
    }


    /**
     * <p>Get unit-version repo.</p>
     * @return the unitVersionRepo
     */
    protected UnitVersionRepo getUnitVersionRepo() {
        return unitVersionRepo;
    }

}

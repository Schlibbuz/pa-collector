"""
Testing sqlalchemy
"""
import os
from sqlalchemy import Column, Integer, String, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


Base = declarative_base()
Session = sessionmaker()

class Giraf(Base): # paylint: disable=too-few-public-methods
    """Maps to giraf table"""

    __tablename__ = 'girafs'

    id = Column(Integer, primary_key=True)
    name = Column(String(50))

    def __repr__(self):
        """text repr"""

        return "<Giraf(id=%s, name='%s')>" % (self.id, self.name)


def init():
    """init db"""

    engine = create_engine(
        'postgresql+psycopg2://' +
        os.environ['PA_DB_USER'] +
        ':' +
        os.environ['PA_DB_PASS'] +
        '@127.0.0.1:5432/pa',

        echo=True,
    )

    Base.metadata.create_all(engine)
    Session.configure(bind=engine)


def main():
    """main function"""
    init()

    tony_giraf = Giraf(name='Tony')
    session = Session()
    session.add(tony_giraf)
    session.commit()
    our_giraf = session.query(Giraf).filter_by(name='Tony').first()
    print(our_giraf)


if __name__ == "__main__":
    main()

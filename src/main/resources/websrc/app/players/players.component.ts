import { Component, OnInit, Input } from '@angular/core';

import { Player } from 'app/players/player';
@Component({
  selector: 'app-player',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.scss']
})
export class PlayersComponent implements OnInit {

    @Input() player: Player;
  constructor() { }

  ngOnInit(): void {

  }

}

require('dotenv');
const { Client } = require('pg')
const MongoTools = require('./mongo-tools')

const client = new Client({
    connectionString: process.env.PA_PG_URL,
})

const mongoTools = new MongoTools();

    ; (async () => {
        await client.connect()
        const pgResult = await client.query('SELECT * FROM versions')
        // pgResult.rows.forEach((row, index) => {
        //     pgResult.rows[index].json_data = JSON.parse(row.json_data)
        // })
        mongoTools.client()
        .then(client => {

            const db = client.db('pa');
            const coll = db.collection('versions');
            coll.insertMany(pgResult.rows).then(result => {
                console.log(result);
            });
        })
        .catch(err => {
            console.log(err)
        });
        await client.end()
    })()

class PgTools {
    // mongoClient = null;
    // mongoUrl = process.env.PA_PG_URL;

    // constructor() { }

    async client() {
        await client.connect()
        const pgResult = await client.query('SELECT * FROM units_versions')
        mongoTools.client()
        .then(client => {

            const db = client.db('pa');
            const coll = db.collection('units_versions');
            coll.insertMany(pgResult.rows).then(result => {
                console.log(result);
            });
        })
        .catch(err => {
            console.log(err)
        });
        await client.end()
    }

    // async open() {
    //     return await mongoClient.connect(this.mongoUrl);
    // }
}

module.exports = PgTools;

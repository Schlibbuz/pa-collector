import { Component, OnInit } from '@angular/core';

import { Versions } from 'environments/versions';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

    versions = Versions;
    constructor() { }

    ngOnInit(): void {
    }

}

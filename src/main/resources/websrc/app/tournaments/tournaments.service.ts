import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';


import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from 'environments/environment';
import { Tournament } from './tournament';
import { HttpErrorHandler, HandleError } from '../http-error-handler.service';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'my-auth-token'
    })
};

@Injectable()
export class TournamentsService {
    tournamentsUrl = `${environment.apiUrl}/api/tournaments`;  // URL to web api

    private handleError: HandleError;

    constructor(
        private http: HttpClient,
        httpErrorHandler: HttpErrorHandler) {
        this.handleError = httpErrorHandler.createHandleError('TournamentsService');
    }

    /** GET tournaments from the server */
    getTournaments(): Observable<Tournament[]> {
        return this.http.get<Tournament[]>(this.tournamentsUrl)
            .pipe(
                catchError(this.handleError('getTournaments', []))
            );
    }

    /* GET tournaments whose name contains search term */
    searchTournaments(term: string): Observable<Tournament[]> {
        term = term.trim();

        // Add safe, URL encoded search parameter if there is a search term
        const options = term ?
            { params: new HttpParams().set('name', term) } : {};

        return this.http.get<Tournament[]>(this.tournamentsUrl, options)
            .pipe(
                catchError(this.handleError<Tournament[]>('searchTournaments', []))
            );
    }

    //////// Save methods //////////

    /** POST: add a new tournament to the database */
    addTournament(tournament: Tournament): Observable<Tournament> {
        return this.http.post<Tournament>(this.tournamentsUrl, tournament, httpOptions)
            .pipe(
                catchError(this.handleError('addTournament', tournament))
            );
    }

    /** DELETE: delete the tournament from the server */
    deleteTournament(id: string): Observable<{}> {
        const url = `${this.tournamentsUrl}/${id}`; // DELETE api/tournaments/xyz
        return this.http.delete(url, httpOptions)
            .pipe(
                catchError(this.handleError('deleteTournament'))
            );
    }

    /** PUT: update the tournament on the server. Returns the updated tournament upon success. */
    updateTournament(tournament: Tournament): Observable<Tournament> {
        httpOptions.headers =
            httpOptions.headers.set('Authorization', 'my-new-auth-token');

        return this.http.put<Tournament>(this.tournamentsUrl, tournament, httpOptions)
            .pipe(
                catchError(this.handleError('updateTournament', tournament))
            );
    }
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/

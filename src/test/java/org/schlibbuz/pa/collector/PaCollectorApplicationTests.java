package org.schlibbuz.pa.collector;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


/**
 * <p>App-tests.</p>
 * @author Stefan
 */
@SpringBootTest
class PaCollectorApplicationTests {


    /**
     * <p>Tests if context is loadable.</p>
     */
    @Test
    void contextLoads() {
    }

}

import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from 'app/_helpers/auth.guard';

import { AboutComponent } from 'app/about/about.component';
import { AdminComponent } from 'app/admin/admin.component';
import { HomeComponent } from 'app/home/home.component';
import { RootComponent } from 'app/root/root.component';
import { UnitsDbComponent } from 'app/units-db/units-db.component';
import { LoginComponent } from 'app/login/login.component';
import { UsersComponent } from 'app/users/users.component';
import { ProfileComponent } from 'app/users/profile/profile.component';


const routes: Routes = [
    { path: '', component: RootComponent },
    { path: 'about', component: AboutComponent },
    { path: 'admin', component: AdminComponent, canActivate: [AuthGuard]  },
    { path: 'home', component: HomeComponent, canActivate: [AuthGuard]  },
    { path: 'login', component: LoginComponent },
    { path: 'units', component: UnitsDbComponent },
];

@NgModule({
    imports: [
        BrowserModule,
        RouterModule.forRoot(routes),
        ReactiveFormsModule,
    ],
    exports: [RouterModule],
    declarations: [
        AboutComponent,
        HomeComponent,
        RootComponent,
        UnitsDbComponent,
        LoginComponent,
        AdminComponent,
        ProfileComponent,
        UsersComponent,
    ],
})
export class AppRoutingModule { }

package org.schlibbuz.pa.collector.jpa;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

/**
 * <p>UnitVersion entity.</p>
 * @author Stefan
 */
@Entity
@Table(name = "units_versions")
public class UnitVersion implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * <p>Composite-Primary.</p>
     */
    @EmbeddedId
    private UnitVersionId unitVersionId;


    /**
     * <p>Unit relation.</p>
     */
    @MapsId("unitId")
    @ManyToOne
    private Unit unit;


    /**
     * <p>Version relation.</p>
     */
    @MapsId("versionId")
    @ManyToOne
    private Version version;


    /**
     * <p>Column for json-data.</p>
     */
    @Column(nullable = false, columnDefinition = "TEXT")
    private String jsonData;


    /**
     * <p>Default-Constructor.</p>
     */
    public UnitVersion() {

    }


    /**
     * <p>Constructor.</p>
     * @param unitVersionId the unitVersionId
     */
    public UnitVersion(final UnitVersionId unitVersionId) {
        this.unitVersionId = unitVersionId;

    }


    /**
     * <p>Constructor.</p>
     * @param unitVersionId the unitVersionId
     * @param jsonData the json-data
     */
    public UnitVersion(final UnitVersionId unitVersionId, final String jsonData) {
        this.unitVersionId = unitVersionId;
        this.jsonData = jsonData;

    }


    /**
     * <p>Gets the composite-key.</p>
     * @return the composite-key
     */
    public UnitVersionId getUnitVersionId() {
        return unitVersionId;
    }


    /**
     * <p>Set the composite-key.</p>
     * @param unitVersionId the unitVersionId
     */
    public void setUnitVersionId(final UnitVersionId unitVersionId) {
        this.unitVersionId = unitVersionId;
    }


    /**
     * <p>Getter.</p>
     * @return the unit
     */
    public Unit getUnit() {
        return unit;
    }


    /**
     * <p>Setter.</p>
     * @param unit the unit
     */
    public void setUnit(final Unit unit) {
        this.unit = unit;
    }


    /**
     * <p>Getter.</p>
     * @return the version
     */
    public Version getVersion() {
        return version;
    }


    /**
     * <p>Setter.</p>
     * @param version the version
     */
    public void setVersion(final Version version) {
        this.version = version;
    }


    /**
     * <p>Getter.</p>
     * @return the json-data
     */
    public String getJsonData() {
        return jsonData;
    }


    /**
     * <p>Setter.</p>
     * @param jsonData the json-data
     */
    public void setJsonData(final String jsonData) {
        this.jsonData = jsonData;
    }


    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.unitVersionId);
        hash = 37 * hash + Objects.hashCode(this.unit);
        hash = 37 * hash + Objects.hashCode(this.version);
        hash = 37 * hash + Objects.hashCode(this.jsonData);
        return hash;
    }


    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnitVersion other = (UnitVersion) obj;
        if (!Objects.equals(this.jsonData, other.jsonData)) {
            return false;
        }
        if (!Objects.equals(this.unitVersionId, other.unitVersionId)) {
            return false;
        }
        if (!Objects.equals(this.unit, other.unit)) {
            return false;
        }
        if (!Objects.equals(this.version, other.version)) {
            return false;
        }
        return true;
    }


    @Override
    public String toString() {
        return "UnitVersion{" + "unitVersionId=" + unitVersionId
                + ", unit=" + unit
                + ", version=" + version
                + ", jsonData=" + jsonData
                + "}";
    }

}

import { Component, OnInit, Input } from '@angular/core';

import { Tournament } from 'app/tournaments/tournament';
import { Player } from 'app/players/player';


const roundsDef = [
    'Finals',
    'Semies',
    'Round of 8',
    'Round of 16',
    'Round of 32',
];

@Component({
    selector: 'app-tournament-bracket',
    templateUrl: './brackets.component.html',
    styleUrls: ['./brackets.component.scss']
})
export class BracketsComponent implements OnInit {

    @Input() tournament: Tournament;

    constructor() { }

    ngOnInit(): void {

    }

    hasWinner(): boolean {
        return Object.keys(this.tournament.bracket).length > 0
               && this.tournament.bracket.constructor === Object;
    }

}

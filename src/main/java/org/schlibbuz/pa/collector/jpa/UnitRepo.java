/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.pa.collector.jpa;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 * <p>Use to crud unit-entities.</p>
 * @author Stefan
 */
public interface UnitRepo extends CrudRepository<Unit, Long> {


    /**
     * <p>Find unit by id.</p>
     * @param id the id
     * @return the result or null
     */
    Unit findById(long id);


    /**
     * <p>find units by name.</p>
     * @param name the name
     * @return the results or null
     */
    List<Unit> findByName(String name);


    /**
     * <p>Find units by slug.</p>
     * @param slug the slug
     * @return the results or null
     */
    List<Unit> findBySlug(String slug);
}

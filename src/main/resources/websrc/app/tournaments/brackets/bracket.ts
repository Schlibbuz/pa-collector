import { Round } from './rounds/round';

export interface Bracket {
    rounds: Array<Round>;
}

'use strict';
const MongoTools = require('./mongo-tools');

const mongoTools = new MongoTools();

exports.getAllTournaments = function(req, res) {
    console.log(req.url);
    res.setHeader("encoding", "utf8");
    res.setHeader("Content-Type", "application/json");
    res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Request-Method', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');

    mongoTools.client()
    .then(client => {

        const db = client.db('pa');
        const coll = db.collection('tournaments');
        coll.find({}).toArray().then(result => {
            res.write(JSON.stringify(result));
            res.end();
        });
    })
    .catch(err => {
        console.log(err)
        res.write(JSON.stringify(Constants));
        res.end();
    });

};



exports.getTournament = function(req, res) {
    console.log(req.url);
    res.setHeader("encoding", "utf8");
    res.setHeader("Content-Type", "application/json");
    res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Request-Method', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');

    mongoTools.client()
    .then(client => {

        const db = client.db('pa');
        const coll = db.collection('tournaments');
        coll.find({}).toArray().then(result => {
            res.write(JSON.stringify(result));
            res.end();
        });
    })
    .catch(err => {
        console.log(err)
        res.write(JSON.stringify(Constants));
        res.end();
    });
};

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.pa.collector;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * <p>Holds the status/health of the scraper.manager.</p>
 * @author Stefan
 */
public class ScraperManagerStatus {


    /**
     * <p>Tells if the manager has capacity for more tasks.</p>
     */
    private boolean hasCapacity;


    /**
     * <p>Detailed status/health of each active worker.</p>
     */
    private static final Map<UUID, ScraperWorkerStatus> SCRAPER_WORKER_STATI = new HashMap<>();


    /**
     * <p>Default-Constructor.</p>
     */
    public ScraperManagerStatus() {
        hasCapacity = false;
    }


    /**
     * <p>Constructor.</p>
     * @param hasCapacity initialize/override capacity-switch
     */
    public ScraperManagerStatus(final boolean hasCapacity) {
        this.hasCapacity = hasCapacity;
    }


    /**
     * <p>Set capacity-switch.</p>
     * @param capacity the capacity-switch
     * @return instance for chaining
     */
    public ScraperManagerStatus hasCapacity(final boolean capacity) {
        hasCapacity = capacity;

        return this;
    }


    /**
     * <p>Getter.</p>
     * @return the capacity-switch
     */
    public boolean getHasCapacity() {
        return hasCapacity;
    }


    /**
     * <p>Setter.</p>
     * @param hasCapacity the capacity-switch
     */
    public void setHasCapacity(final boolean hasCapacity) {
        this.hasCapacity = hasCapacity;
    }


    /**
     * <p>Get the map with the worker-stati.</p>
     * @return the map of interest
     */
    public Map<UUID, ScraperWorkerStatus> getScraperWorkerStati() {
        return SCRAPER_WORKER_STATI;
    }


    /**
     * <p>Publish status-data of a scrape-task.</p>
     * @param uuid the uuid of the scrape-task
     * @param status the status-table of the scrape-task
     */
    public void addScraperWorkerStatus(final UUID uuid, final ScraperWorkerStatus status) {
        SCRAPER_WORKER_STATI.put(uuid, status);
    }


    /**
     * <p>Get the status-data of a scrape-task.</p>
     * @param uuid the uuid of the scrape-task
     * @return the status-table of the task with given uuid
     */
    public ScraperWorkerStatus getScraperWorkerStatus(final UUID uuid) {
        return SCRAPER_WORKER_STATI.get(uuid);
    }


    /**
     * <p>Archive a finished/cancelled scrape-task.</p>
     * @param uuid the uuid of the scrape-task
     */
    public void archiveScraperWorkerStatus(final UUID uuid) {
        SCRAPER_WORKER_STATI.remove(uuid);
    }

}

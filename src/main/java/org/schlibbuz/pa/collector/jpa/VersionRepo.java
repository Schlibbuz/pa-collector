/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.pa.collector.jpa;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * <p>Version repository. Is a REST-Resource.</p>
 * @author Stefan
 */
@RepositoryRestResource(collectionResourceRel = "versions", path = "versions")
public interface VersionRepo extends CrudRepository<Version, Long> {


    /**
     * <p>Find a version by id.</p>
     * @param id the id
     * @return result or null
     */
    Version findById(long id);


    /**
     * <p>Find a version by name.</p>
     * @param name the name
     * @return results
     */
    List<Version> findByName(String name);
}

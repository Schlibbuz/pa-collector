/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.pa.collector;


/**
 * <p>Controls access to ScraperService.</p>
 * @author Stefan
 */
public interface IScraperService {


    /**
     * <p>Start a scrape-run. Succeeds if ScraperManager has capacity.</p>
     * @return the status
     */
    ScraperManagerStatus start();


    /**
     * <p>Get the status of current scrape-runs.</p>
     * @return the status
     */
    ScraperManagerStatus status();
}

export interface Deactivation {
    created: Date;
    reason: string;
}

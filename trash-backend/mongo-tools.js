require('dotenv');
const MongoClient = require('mongodb').MongoClient;

class MongoTools {
    mongoClient = null;
    mongoUrl = 'mongodb://192.168.0.11:27017';

    constructor() { }

    async client() {
        if (this.mongoClient && this.mongoClient.isConnected()) {
            return this.mongoClient;
        } else {
            console.log(`Connecting to ${this.mongoUrl}...`)
            this.mongoClient = await MongoClient.connect(this.mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true })
            console.log("Connected to database.");
            return this.mongoClient;
        }
    }

    async open() {
        return await mongoClient.connect(this.mongoUrl);
    }
}

module.exports = MongoTools;

const express = require('express');

const MongoTools = require('./mongo-tools');

const app = express();
const PORT = 4220;
const mongoTools = new MongoTools();


app.get('/api/tournaments', (req, res) => {
    console.log(req.url);
    res.setHeader("encoding", "utf8");
    res.setHeader("Content-Type", "application/json");
    res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Request-Method', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');

    mongoTools.client()
    .then(client => {

        const db = client.db('pa');
        const coll = db.collection('tournaments');
        coll.find({}).sort({ created: -1 }).toArray().then(result => {
            res.write(JSON.stringify(result));
            res.end();
        });
    })
    .catch(err => {
        console.log(err)
        res.write(JSON.stringify(Constants));
        res.end();
    });
});

app.get('/api/units', (req, res) => {
    console.log(req.url);
    res.setHeader("encoding", "utf8");
    res.setHeader("Content-Type", "application/json");
    res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Request-Method', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');

    mongoTools.client()
    .then(client => {

        const db = client.db('pa');
        const coll = db.collection('units');
        coll.find({}).toArray().then(result => {
            res.write(JSON.stringify(result));
            res.end();
        });
    })
    .catch(err => {
        console.log(err)
        res.write(JSON.stringify(Constants));
        res.end();
    });
});

app.get('/api/versions', (req, res) => {
    console.log(req.url);
    res.setHeader("encoding", "utf8");
    res.setHeader("Content-Type", "application/json");
    res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Request-Method', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');

    mongoTools.client()
    .then(client => {

        const db = client.db('pa');
        const coll = db.collection('versions');
        coll.find({}).toArray().then(result => {
            res.write(JSON.stringify(result));
            res.end();
        });
    })
    .catch(err => {
        console.log(err)
        res.write(JSON.stringify(Constants));
        res.end();
    });
});

app.listen(PORT, () => {
    console.log('API-Server started on port ' + PORT);
});

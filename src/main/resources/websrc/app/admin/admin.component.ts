import { Component, OnInit } from '@angular/core';

import { User } from 'app/_models/user';
import { UserService } from 'app/_services/user.service';

@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

    users: Array<User>;
    constructor(
        private userService: UserService,
    ) { }

    ngOnInit(): void {
        this.getAllUsers();
    }

    getAllUsers(): void {
        this.userService.getAll()
            .subscribe(users => (this.users = users));
    }

}

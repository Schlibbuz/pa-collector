import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientXsrfModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { HttpErrorHandler } from './http-error-handler.service';
import { MessageService } from './message.service';

import { AppComponent } from 'app/app.component';
import { AppRoutingModule } from 'app/app.routing.module';
import { BracketsComponent } from 'app/tournaments/brackets/brackets.component';
import { ModalComponent } from 'app/modal/modal.component';
import { NavbarComponent } from 'app/navbar/navbar.component';
import { PairingsComponent } from 'app/tournaments/brackets/rounds/pairings/pairings.component';
import { PlayersComponent } from 'app/players/players.component';
import { RainbowComponent } from 'app/rainbow/rainbow.component';
import { RoundsComponent } from 'app/tournaments/brackets/rounds/rounds.component';
import { TeamsComponent } from 'app/teams/teams.component';
import { TournamentsComponent } from 'app/tournaments/tournaments.component';
import { UnitsComponent } from 'app/units/units.component';
import { SplashComponent } from 'app/splash/splash.component';
import { DiscordLinkComponent } from 'app/discord-link/discord-link.component';
import { FooterComponent } from 'app/footer/footer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { JwtInterceptor } from 'app/_helpers/jwt.interceptor';
import { ErrorInterceptor } from 'app/_helpers/error.interceptor';


@NgModule({
    declarations: [
        AppComponent,
        BracketsComponent,
        ModalComponent,
        NavbarComponent,
        PairingsComponent,
        PlayersComponent,
        RainbowComponent,
        RoundsComponent,
        TeamsComponent,
        TournamentsComponent,
        UnitsComponent,
        SplashComponent,
        DiscordLinkComponent,
        FooterComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        HttpClientXsrfModule.withOptions({
            cookieName: 'My-Xsrf-Cookie',
            headerName: 'My-Xsrf-Header',
        }),
        NgbModule,
        BrowserAnimationsModule,
    ],
    providers: [
        HttpErrorHandler,
        MessageService,
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }

import { Team } from 'app/teams/team';

export interface Pairing {
    teams: Array<Team>;
}
